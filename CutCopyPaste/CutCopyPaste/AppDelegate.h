//
//  AppDelegate.h
//  DexsatiFramwork
//
//  Created by TIKAM CHANDRAKAR on 30/05/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MainViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) IBOutlet UIWindow *window;


@property (strong, nonatomic) UIImage *stickerImage;



@property (strong, nonatomic) MainViewController *mainViewController;
@property (nonatomic,strong) UIAlertView *alertView;

@end

