//
//  ConstantString.h
//  DexsatiFramwork
//
//  Created by TIKAM CHANDRAKAR on 01/06/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#ifndef DexsatiFramwork_ConstantString_h
#define DexsatiFramwork_ConstantString_h

#define kSettingsMessageGallery @"Please go to Privacy and change the settings to use Gallery"
#define kSettingsMessageCamera @"Please go to Privacy and change the settings to use Camera"

#endif
