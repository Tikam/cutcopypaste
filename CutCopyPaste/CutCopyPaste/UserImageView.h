//
//  UserImageView.h
//  DexsatiFramwork
//
//  Created by TIKAM CHANDRAKAR on 01/06/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//


#import <UIKit/UIKit.h>
enum
{
    ENUM_IMAGE_DRAW_IMAGE_VIEW,
    ENUM_MOVED,
    ENUM_CLIPPED,
    ENUM_FINISHED,
    ENUM_TEST_CODE
};

@interface UserImageView : UIImageView
{
    NSMutableArray *points;
    NSMutableArray *pointsPath;
    CGMutablePathRef myPath;
    BOOL isClipped;
    UIImage* orignalImage;
    UIImage* clippedImage;
    NSInteger status;
}

@property (weak, nonatomic) IBOutlet UIImageView *imageView;


@end