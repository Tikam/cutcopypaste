//
//  CropperViewController.m
//  DexsatiFramwork
//
//  Created by TIKAM CHANDRAKAR on 30/05/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import "CropperViewController.h"
#import "CropperView.h"
#import "Utility.h"

@interface CropperViewController ()

@end

@implementation CropperViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
  _cropView = [[CropperView alloc] initWithFrame:CGRectMake(20 , 77,280, 284)];
//    
//    _cropView.userInteractionEnabled = YES;
//    _cropView.contentMode = UIViewContentModeCenter;
//    
//   
//
    //[self.view addSubview:_cropView];
    
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backButtonClicked:(id)sender
{    
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)takePhoto:(UIButton *)sender {
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"
                                                              message:@"Device has no camera"
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles: nil];
        
        [myAlertView show];
        
    }
    
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    
    // authorized
    if (authStatus == AVAuthorizationStatusAuthorized)
    {
        [self openCamera];
    }
    // denied or restricted
    else if (authStatus == AVAuthorizationStatusDenied || authStatus == AVAuthorizationStatusRestricted)
    {
        [Utility showAlertViewWithTitle:[Utility appName]
                               message:kSettingsMessageCamera
                              delegate:self
                          buttonTitles:[NSArray arrayWithObjects:@"Ok", nil]
                     cancelButtonIndex:0
                              tagValue:1];
    }
    // not determined, we should ask peremisison
    else if (authStatus == AVAuthorizationStatusNotDetermined)
    {
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted)
         {
             if (granted)
             {
                 // show not be invoked from main thread
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [self openCamera];
                 });
             }
             else
             {
                 // show not be invoked from main thread
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [Utility showAlertViewWithTitle:[Utility appName]
                                            message:kSettingsMessageCamera
                                           delegate:self
                                       buttonTitles:[NSArray arrayWithObjects:@"Ok", nil]
                                  cancelButtonIndex:0
                                           tagValue:1];
                 });
             }
         }];
    }
    else
    {
        [Utility showAlertViewWithTitle:[Utility appName]
                               message:kSettingsMessageCamera
                              delegate:self
                          buttonTitles:[NSArray arrayWithObjects:@"Ok", nil]
                     cancelButtonIndex:0
                              tagValue:1];
    }
   
    
}


- (IBAction)openCamera
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:picker animated:YES completion:NULL];
}

- (IBAction)selectPhoto:(UIButton *)sender {
   
    
    ALAuthorizationStatus autorizationStatus = [ALAssetsLibrary authorizationStatus];
    
    // autorized
    if (autorizationStatus == ALAuthorizationStatusAuthorized)
    {
        [self openGallery];
    }
    // denied or restricted
    else if (autorizationStatus == ALAuthorizationStatusDenied || autorizationStatus == ALAuthorizationStatusRestricted)
    {
        [Utility showAlertViewWithTitle:[Utility appName]
                               message:kSettingsMessageGallery
                              delegate:self
                          buttonTitles:[NSArray arrayWithObjects:@"Ok", nil]
                     cancelButtonIndex:0
                              tagValue:1];
    }
    // not determined, we should ask peremisison
    else if (autorizationStatus == ALAuthorizationStatusNotDetermined)
    {
        ALAssetsLibrary *lib = [[ALAssetsLibrary alloc] init];
        [lib enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos usingBlock:^(ALAssetsGroup *group, BOOL *stop)
         {
             if (group)
             {
                 [self performSelector:@selector(openGallery) withObject:self afterDelay:1];
             }
             
         } failureBlock:^(NSError *error)
         {
             // show not be invoked from main thread
             dispatch_async(dispatch_get_main_queue(), ^{
                 [Utility showAlertViewWithTitle:[Utility appName]
                                        message:kSettingsMessageGallery
                                       delegate:self
                                   buttonTitles:[NSArray arrayWithObjects:@"Ok", nil]
                              cancelButtonIndex:0
                                       tagValue:1];
             });
         }];
    }
    
    
}

- (IBAction)openGallery
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
}

#pragma mark - Image Picker Controller delegate methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    self.imageView.image = chosenImage;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

@end
