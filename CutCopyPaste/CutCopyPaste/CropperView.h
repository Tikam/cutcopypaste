//
//  CropperView.h
//  DexsatiFramwork
//
//  Created by TIKAM CHANDRAKAR on 30/05/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import <UIKit/UIKit.h>

enum
{
    ENUM_IMAGE_DRAW,
    ENUM_MOVED,
    ENUM_CLIPPED,
    ENUM_FINISHED,
    ENUM_TEST_CODE
};

@interface CropperView : UIView{
    NSMutableArray *points;    
    NSMutableArray *pointsPath;
    CGMutablePathRef myPath;
    BOOL isClipped;
    UIImage* orignalImage;
    UIImage* clippedImage;
    NSInteger status;
}

@property (weak, nonatomic) IBOutlet UIImageView *imageView;


@end
