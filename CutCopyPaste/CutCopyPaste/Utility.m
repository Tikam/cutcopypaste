//
//  Utility.m
//  DexsatiFramwork
//
//  Created by TIKAM CHANDRAKAR on 01/06/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import "Utility.h"
#import "AppDelegate.h"



AppDelegate* GetAppDelegate()
{
    return (AppDelegate*)[[UIApplication sharedApplication] delegate];
}
@implementation Utility

+ (void)showAlertViewWithTitle:(NSString*)title
                       message:(NSString*)message
                      delegate:(id <UIAlertViewDelegate>)delegate
                  buttonTitles:(NSArray*)buttonTitles
             cancelButtonIndex:(int)cancelButtonIndex
{
    
    
    if (GetAppDelegate().alertView != nil)
    {
        GetAppDelegate().alertView.delegate = nil;
        GetAppDelegate().alertView = nil;
    }
    
    GetAppDelegate().alertView = [[UIAlertView alloc] initWithTitle:title
                                                            message:message
                                                           delegate:delegate
                                                  cancelButtonTitle:nil
                                                  otherButtonTitles:nil];
    for (NSString *title in buttonTitles)
    {
        [GetAppDelegate().alertView addButtonWithTitle: title];
    }
    GetAppDelegate().alertView.cancelButtonIndex = cancelButtonIndex;
    [GetAppDelegate().alertView show];
}

+ (void)showAlertViewWithTitle:(NSString*)title
                       message:(NSString*)message
                      delegate:(id <UIAlertViewDelegate>)delegate
                  buttonTitles:(NSArray*)buttonTitles
             cancelButtonIndex:(int)cancelButtonIndex
                      tagValue:(int)tag
{
    
    if (GetAppDelegate().alertView != nil)
    {
        GetAppDelegate().alertView.delegate = nil;
        GetAppDelegate().alertView = nil;
    }
    
    GetAppDelegate().alertView = nil;
    GetAppDelegate().alertView	= [[UIAlertView alloc] initWithTitle:title
                                                            message:message
                                                           delegate:delegate
                                                  cancelButtonTitle:nil
                                                  otherButtonTitles:nil];
    
    [GetAppDelegate().alertView setTag:tag];
    
    for (NSString *title in buttonTitles)
    {
        [GetAppDelegate().alertView addButtonWithTitle: title];
    }
    
    GetAppDelegate().alertView.cancelButtonIndex = cancelButtonIndex;
    
    [GetAppDelegate().alertView show];
}
+ (NSString*)appName
{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
}

@end
