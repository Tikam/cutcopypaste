//
//  CropperView.m
//  DexsatiFramwork
//
//  Created by TIKAM CHANDRAKAR on 30/05/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import "CropperView.h"

@implementation CropperView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
       
    }
    return self;
}


- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
   
    switch (status) {
        case ENUM_IMAGE_DRAW:
        {
            orignalImage = [UIImage imageNamed:@"DSC06267.JPG"];
            CGRect imageRect = CGRectMake(0, 0, self.frame.size.width,self.frame.size.height);
            CGContextTranslateCTM(context, 0, imageRect.size.height);
            CGContextScaleCTM(context, 1.0, -1.0);
            CGContextDrawImage(context, imageRect, orignalImage.CGImage);
            
        }
            break;
        case ENUM_MOVED:
        {
            orignalImage = [UIImage imageNamed:@"DSC06267.JPG"];
            CGRect imageRect = CGRectMake(0, 0, self.frame.size.width,self.frame.size.height);
            CGContextTranslateCTM(context, 0, imageRect.size.height);
            CGContextScaleCTM(context, 1.0, -1.0);
            CGContextDrawImage(context, imageRect, orignalImage.CGImage);
            if(points.count > 0)
            {
                
                CGContextBeginPath(context);
                CGPoint myStartPoint= [(NSValue *)[pointsPath objectAtIndex:0] CGPointValue];
                CGContextMoveToPoint(context, myStartPoint.x,self.frame.size.height- myStartPoint.y);
                
                for (int j=0; j<[points count]-1; j++)
                {
                    CGPoint myEndPoint = [(NSValue *)[pointsPath objectAtIndex:j+1] CGPointValue];
                    //--------------------------------------------------------
                    CGContextAddLineToPoint(context, myEndPoint.x,self.frame.size.height-myEndPoint.y);
                }
                
                UIColor *lineColor=[UIColor blackColor];
                float width=3.0;
                CGContextSetStrokeColorWithColor(context,[lineColor CGColor]);
                //-------------------------------------------------------
                CGContextSetLineWidth(context, width);
                CGContextStrokePath(context);
            }

        }
            break;
        case ENUM_CLIPPED:
        {
               if (pointsPath && pointsPath.count > 0) {
                CGPoint p = [(NSValue *)[pointsPath objectAtIndex:0] CGPointValue];
                
                p = CGPointMake(p.x, p.y);
                
                CGPathMoveToPoint(myPath, nil, p.x, p.y);
                
                for (int i = 1; i < pointsPath.count; i++) {
                    p = [(NSValue *)[pointsPath objectAtIndex:i] CGPointValue];
                    p = CGPointMake(p.x, p.y);
                    CGPathAddLineToPoint(myPath, nil, p.x,  p.y);
                }
            }
            CGContextAddPath(context, myPath);
            CGContextClip(context);
            CGPathRelease(myPath);
            [orignalImage drawInRect:[self bounds]];
            
            CGImageRef imageRef = CGBitmapContextCreateImage (context);
            UIImage *newImage = [UIImage imageWithCGImage:imageRef];
            
        }
            break;
        case ENUM_TEST_CODE:
        {
            UIGraphicsBeginImageContextWithOptions(self.frame.size, NO, 0.0);
            CGContextBeginPath (context);
            CGFloat height = self.bounds.size.height;
            CGContextTranslateCTM(context, 0.0, height);
            CGContextScaleCTM(context, 1.0, -1.0);
            
            
            if (pointsPath && pointsPath.count > 0) {
                CGPoint p = [(NSValue *)[pointsPath objectAtIndex:0] CGPointValue];
                
                p = CGPointMake(p.x, self.frame.size.height-p.y);
                
                CGPathMoveToPoint(myPath, nil, p.x, p.y);
                
                for (int i = 1; i < pointsPath.count; i++) {
                    p = [(NSValue *)[pointsPath objectAtIndex:i] CGPointValue];
                    p = CGPointMake(p.x, p.y);
                    CGPathAddLineToPoint(myPath, nil, p.x,  self.frame.size.height-p.y);
                }
            }
            CGContextAddPath(context, myPath);
            
            CGContextClosePath(context);
            CGContextSaveGState(context);
            //CGContextClipToRect(context,CGRectMake(0, 0,100,100));
            
            CGContextClip(context);
            CGPathRelease(myPath);
            
            CGContextDrawImage(context, CGRectMake(0,0,self.frame.size.width, self.frame.size.height), orignalImage.CGImage);
            CGImageRef imageRef = CGBitmapContextCreateImage (context);
           clippedImage = [UIImage imageWithCGImage:imageRef];
           CGContextRestoreGState(context);
           NSLog(@"Clipped Image %f",clippedImage.size.width);
           [self saveImage:clippedImage];
            
        }
       
            break;
            
        default:
        {
            return;
        }
            break;
    }
  
    
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    points = [NSMutableArray array];
    UITouch *touch = [touches anyObject];
    pointsPath = [NSMutableArray array];
    CGPoint touchPosition = [touch locationInView:self];
    [pointsPath addObject:[NSValue valueWithCGPoint:touchPosition]];
    
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch* touch=[touches anyObject];
    myPath = CGPathCreateMutable();
    CGPoint touchPosition = [touch locationInView:self];
    [points addObject:[NSValue valueWithCGPoint:touchPosition]];
    [pointsPath addObject:[NSValue valueWithCGPoint:touchPosition]];
    status = ENUM_MOVED;
    [self setNeedsDisplay];
    
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    //[self orignalImage:backgroundImage withCGPath:myPath];
    //isClipped = YES;
    
    status = ENUM_TEST_CODE;
    [self setNeedsDisplay];
    //status = ENUM_FINISHED;
    points = nil;
}
- (void)saveImage: (UIImage*)image
{
    if (image != nil)
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                             NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString* path = [documentsDirectory stringByAppendingPathComponent:
                          @"test.png" ];
        NSData* data = UIImagePNGRepresentation(image);
        [data writeToFile:path atomically:YES];
    }
}
//The following function loads UIImage from the test.png file:

- (UIImage*)loadImage
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                         NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:
                      @"test.png" ];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    return image;
}











@end
