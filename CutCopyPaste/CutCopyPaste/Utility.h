//
//  Utility.h
//  DexsatiFramwork
//
//  Created by TIKAM CHANDRAKAR on 01/06/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"

AppDelegate* GetAppDelegate();

@interface Utility : NSObject
{
    
}
+ (void)showAlertViewWithTitle:(NSString*)title
                       message:(NSString*)message
                      delegate:(id <UIAlertViewDelegate>)delegate
                  buttonTitles:(NSArray*)buttonTitles
             cancelButtonIndex:(int)cancelButtonIndex;

+ (void)showAlertViewWithTitle:(NSString*)title
                       message:(NSString*)message
                      delegate:(id <UIAlertViewDelegate>)delegate
                  buttonTitles:(NSArray*)buttonTitles
             cancelButtonIndex:(int)cancelButtonIndex
                      tagValue:(int)tag;
+ (NSString*)appName;
@end
