//
//  MainViewController.m
//  DexsatiFramwork
//
//  Created by TIKAM CHANDRAKAR on 30/05/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import "MainViewController.h"
#import "CropperViewController.h"
#import "DisplayViewController.h"

@interface MainViewController ()

@end

@implementation MainViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cropButtonClikced:(id)sender
{
    CropperViewController *crop = [[CropperViewController alloc] init];    
    [self.navigationController pushViewController:crop animated:YES];
}

- (IBAction)displayButtonClikced:(id)sender
{
    DisplayViewController *display = [[DisplayViewController alloc] init];
    [self.navigationController pushViewController:display animated:YES];
}

@end
