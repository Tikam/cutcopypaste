//
//  DisplayViewController.h
//  DexsatiFramwork
//
//  Created by TIKAM CHANDRAKAR on 30/05/15.
//  Copyright (c) 2015 XYMOB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DisplayViewController : UIViewController
@property(nonatomic, strong)IBOutlet UIImageView *imageView;

@end
